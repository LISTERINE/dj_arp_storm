=======
History
=======

1.0b1 (2017-07-17)
------------------

* First release on PyPI.


1.0b2 (2017-07-19)
------------------

* Removed bpf filters because pyshark is blocking
* Shortened sound file names

1.0b2 (2017-07-19)
------------------

* Updated license
* Updated link to source

1.0b3 (2017-07-20)
------------------
* Bug fixes


1.0b4 (2017-07-21)
------------------
* Docs
* New interface options
* Suppress annoying output
